'use strict';

const express = require('express');

const {
  createRoulette,
  openRoulette,
  closeRoulette,
  listRoulettes,
  betRoulette,
} = require('../services/roulette/index');

const router = express.Router();

router.post('/create', (req, res) => createRoulette(res));
router.put('/open/:id', (req, res) => openRoulette(req.params.id, res));
router.put('/close/:id', (req, res) => closeRoulette(req.params.id, res));
router.get('/list', (req, res) => listRoulettes(res));
router.post('/bet', (req, res) => betRoulette(req, res));

module.exports = router;
