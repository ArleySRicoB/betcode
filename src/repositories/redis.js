/* eslint-disable prefer-promise-reject-errors */

'use strict';

const redis = require('redis');
const { Roulette, Bet } = require('../entities');
const HttpRequestConstants = require('../services/constants/HttpRequestConstants');

const client = redis.createClient();

client.on('connect', () => console.log('Connected to Redis...'));
client.on('error', (err) => console.log(`Error: ${err}`));

const saveRoulette = (id, rouletteData) => {
  const roulette = JSON.stringify(rouletteData);
  return new Promise((resolve, reject) => {
    client.hset('roulettes', id, roulette, (error, response) => {
      if (error) reject({ status: HttpRequestConstants.BAD_REQUEST, info: error });
      else resolve({ status: HttpRequestConstants.OK, info: response });
    });
  });
};

const getRoulette = (id) => {
  return new Promise((resolve, reject) => {
    client.hget('roulettes', id, (error, response) => {
      if (error) reject({ status: HttpRequestConstants.BAD_REQUEST, info: error });
      else if (response) {
        const roulette = Object.assign(new Roulette(), JSON.parse(response));
        roulette._bets = roulette._bets.map((bet) => Object.assign(new Bet(), bet));
        resolve({ status: HttpRequestConstants.OK, info: roulette });
      } else reject({ status: HttpRequestConstants.BAD_REQUEST, info: 'Roulette not found' });
    });
  });
};

const getAllRoulettes = () => {
  return new Promise((resolve, reject) => {
    client.hgetall('roulettes', (error, response) => {
      if (error) reject({ status: HttpRequestConstants.BAD_REQUEST, info: error });
      else if (response) {
        const roulettes = Object.keys(response).map((id) => {
          return JSON.parse(response[id]);
        });
        resolve({ status: HttpRequestConstants.OK, info: roulettes });
      } else resolve({ status: HttpRequestConstants.OK, info: [] });
    });
  });
};

module.exports = {
  getAllRoulettes,
  getRoulette,
  saveRoulette,
};
