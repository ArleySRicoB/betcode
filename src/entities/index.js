'use strict';

const Roulette = require('./RouletteEntity');
const Bet = require('./BetEntity');

module.exports = { Roulette, Bet };
