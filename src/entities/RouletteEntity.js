/* eslint-disable no-underscore-dangle */

'use strict';

class Roulette {
  constructor(id, bets, status) {
    this._id = id;
    this._bets = bets;
    this._status = status;
  }

  get id() {
    return this._id;
  }

  set id(id) {
    this._id = id;
  }

  get bets() {
    return this._bets;
  }

  set bets(bet) {
    this._bets = this._bets.concat(bet);
  }

  get status() {
    return this._status;
  }

  set status(status) {
    this._status = status;
  }

  getBetAmount() {
    if (this._bets.length === 0) return 0;
    return this._bets.reduce((acc, bet) => {
      return acc + bet._amount;
    }, 0);
  }
}

module.exports = Roulette;
