'use strict';

class Bet {
  constructor(id, rouletteId, userId, numberSelected, colorSelected, amount) {
    this._id = id;
    this._rouletteId = rouletteId;
    this._userId = userId;
    this._numberSelected = numberSelected;
    this._colorSelected = colorSelected;
    this._amount = amount;
  }

  get id() {
    return this._id;
  }

  set id(id) {
    this._id = id;
  }

  get rouletteId() {
    return this._rouletteId;
  }

  set rouletteId(rouletteId) {
    this._rouletteId = rouletteId;
  }

  get userId() {
    return this._userId;
  }

  set userId(userId) {
    this._userId = userId;
  }

  get numberSelected() {
    return this._numberSelected;
  }

  set numberSelected(numberSelected) {
    this._numberSelected = numberSelected;
  }

  get colorSelected() {
    return this._colorSelected;
  }

  set colorSelected(colorSelected) {
    this._colorSelected = colorSelected;
  }

  get amount() {
    return this._amount;
  }

  set amount(amount) {
    this._amount = amount;
  }
}

module.exports = Bet;
