'use strict';

const { v4: uuidv4 } = require('uuid');
const { Roulette, Bet } = require('../../entities');
const { checkBetFields, checkBetTypes, validateColor } = require('./helper');
const { getRoulette, saveRoulette, getAllRoulettes } = require('../../repositories/redis');

const createRoulette = (res) => {
  const id = uuidv4();
  const rouletteData = new Roulette(id, [], 'created');
  saveRoulette(id, rouletteData)
    .then(({ status }) => {
      res.status(status).json(id);
    })
    .catch(({ status, error }) => {
      res.status(status).json(error);
    });
};

const openRoulette = (id, res) => {
  getRoulette(id)
    .then(async ({ info }) => {
      const newRoulette = info;
      newRoulette._status = 'open';
      await saveRoulette(id, newRoulette)
        .then(({ status }) => {
          res.status(status).json('The roulette was opened successfully');
        })
        .catch(({ status, info: error }) => {
          res.status(status).json(`Failed to open the Roulette ${error}`);
        });
    })
    .catch(({ status, info }) => {
      res.status(status).json(info);
    });
};

const closeRoulette = (id, res) => {
  getRoulette(id)
    .then(async ({ info }) => {
      const newRoulette = info;
      if (newRoulette._status !== 'open') {
        res.status(400).json(`You can not close this roulette because it's no opened`);
      } else {
        newRoulette._status = 'close';
        await saveRoulette(id, newRoulette)
          .then(({ status }) => {
            res
              .status(status)
              .json({ bets: newRoulette.bets, totalAmount: newRoulette.getBetAmount() });
          })
          .catch(({ status, info: error }) => {
            res.status(status).json(`Failed to close the Roulette ${error}`);
          });
      }
    })
    .catch(({ status, info }) => {
      res.status(status).json(info);
    });
};

const saveBet = async (
  { rouletteId, numberSelected, colorSelected, amount },
  userId,
  roulette,
  res,
) => {
  const newRoulette = roulette;
  if (newRoulette._status !== 'open') {
    res.status(400).json(`You cannot bet on this roulette because it's not open`);
  } else {
    const id = uuidv4();
    newRoulette.bets = new Bet(id, rouletteId, userId, numberSelected, colorSelected, amount);
    await saveRoulette(rouletteId, newRoulette)
      .then(({ status }) => {
        res.status(status).json('Bet saved correctly');
      })
      .catch(({ status, info: error }) => {
        res.status(status).json(`Failed to persist the bet: ${error}`);
      });
  }
};

const betRoulette = (req, res) => {
  const userId = req.headers['user-id'];
  if (!checkBetFields(req.body) || !checkBetTypes(req.body)) {
    res.status(404).json('Failed to save the bet. Please check the fields.');
  } else if (!userId) {
    res.status(404).json('Failed to save the bet. User id not found.');
  } else if (!validateColor(req.body.colorSelected)) {
    res.status(404).json('Failed to save the bet. The color is not valid.');
  } else {
    getRoulette(req.body.rouletteId)
      .then(({ info: roulette }) => {
        saveBet(req.body, userId, roulette, res);
      })
      .catch(({ status, info }) => {
        res.status(status).json(info);
      });
  }
};

const listRoulettes = (res) => {
  getAllRoulettes()
    .then(({ info, status }) => {
      res.status(status).json(info);
    })
    .catch(({ status, info }) => {
      res.status(status).json(info);
    });
};

module.exports = {
  createRoulette,
  openRoulette,
  closeRoulette,
  listRoulettes,
  betRoulette,
};
