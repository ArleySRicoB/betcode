'use strict';

const validateColor = (color) => {
  if (!color) return true;
  if (color.toLowerCase() === 'red' || color.toLowerCase() === 'black') return true;
  return false;
};

const checkBetFields = ({ rouletteId, numberSelected, colorSelected, amount }) => {
  if (
    amount &&
    rouletteId &&
    (colorSelected || numberSelected) &&
    numberSelected >= 0 &&
    numberSelected <= 36 &&
    amount > 0 &&
    amount <= 10000
  )
    return true;
  return false;
};

const checkBetTypes = ({ rouletteId, numberSelected, colorSelected, amount }) => {
  if (
    typeof rouletteId === 'string' &&
    typeof numberSelected === 'number' &&
    (typeof colorSelected === 'string' || typeof colorSelected === 'undefined') &&
    (typeof amount === 'number' || typeof amount === 'undefined')
  )
    return true;
  return false;
};

module.exports = {
  checkBetFields,
  checkBetTypes,
  validateColor,
};
