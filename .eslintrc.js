'use strict';

module.exports = {
  extends: ['airbnb-base', 'prettier'],
  plugins: ['import', 'prettier'],
  env: {
    node: true,
  },
  parserOptions: {
    sourceType: 'script',
  },
  rules: {
    strict: ['error', 'safe'],
    'no-console': 'off',
    'no-unused-vars': 'off',
    'no-underscore-dangle': 'off',
    'prettier/prettier': ['error'],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],
  },
};
